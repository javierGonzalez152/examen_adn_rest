package mx.ejercicio.adn.service;

import mx.ejercicio.adn.dto.EstadisticaResponse;
import mx.ejercicio.adn.model.Estadistica;

public interface EstadisticaService {
	public EstadisticaResponse getEstadistica();
	public void updateEstadisticaMutacion();

	public void updateEstadisticaSinMutacion();
}
