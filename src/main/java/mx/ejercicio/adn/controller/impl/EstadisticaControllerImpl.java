package mx.ejercicio.adn.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.ejercicio.adn.controller.EstadisticaController;
import mx.ejercicio.adn.dto.EstadisticaResponse;
import mx.ejercicio.adn.service.EstadisticaService;

@RestController
public class EstadisticaControllerImpl implements EstadisticaController {

	@Autowired
	private EstadisticaService estadisticaService;
	@Override
	@GetMapping("/stats/")
	public ResponseEntity<EstadisticaResponse> getEstadistica() {
		EstadisticaResponse response = estadisticaService.getEstadistica();
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
