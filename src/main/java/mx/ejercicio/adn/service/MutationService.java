package mx.ejercicio.adn.service;

public interface MutationService {
	boolean hasMutation(String[] dna);
}
