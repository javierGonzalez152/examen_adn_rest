package mx.ejercicio.adn.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dna {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer dnaId;
	public Integer getDnaId() {
		return dnaId;
	}
	public void setDnaId(Integer dnaId) {
		this.dnaId = dnaId;
	}
	public String getDnaString() {
		return dnaString;
	}
	public void setDnaString(String dnaString) {
		this.dnaString = dnaString;
	}
	private String dnaString;

}
