package mx.ejercicio.adn.controller;

import org.springframework.http.ResponseEntity;

import mx.ejercicio.adn.dto.EstadisticaResponse;

public interface EstadisticaController {
	ResponseEntity<EstadisticaResponse> getEstadistica();
}
