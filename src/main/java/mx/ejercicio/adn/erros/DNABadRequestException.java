package mx.ejercicio.adn.erros;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DNABadRequestException extends RuntimeException {
	public DNABadRequestException(String message) {
		super(message);
	}
}
