package mx.ejercicio.adn.dto;

public class EstadisticaResponse {
	private Integer countNoMutations;
	private Integer countMutations;
	private Double ratio;
	public Integer getCountNoMutations() {
		return countNoMutations;
	}
	public void setCountNoMutations(Integer countNoMutations) {
		this.countNoMutations = countNoMutations;
	}
	public Integer getCountMutations() {
		return countMutations;
	}
	public void setCountMutations(Integer countMutations) {
		this.countMutations = countMutations;
	}
	public Double getRatio() {
		return ratio;
	}
	public void setRatio(Double ratio) {
		this.ratio = ratio;
	}


}
