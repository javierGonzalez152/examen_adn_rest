package mx.ejercicio.adn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ejercicio.adn.dto.EstadisticaResponse;
import mx.ejercicio.adn.model.Estadistica;
import mx.ejercicio.adn.repository.EstadisticaRepository;
import mx.ejercicio.adn.service.EstadisticaService;

@Service
public class EstadisticaServiceImpl implements EstadisticaService {

	@Autowired
	private EstadisticaRepository estadisticaRepository;

	@Override
	public EstadisticaResponse getEstadistica() {
		// TODO Auto-generated method stub
		EstadisticaResponse response = new EstadisticaResponse();
		Estadistica estadistica = findEstadistica();

		response.setCountMutations(estadistica.getCountMutations());
		response.setCountNoMutations(estadistica.getCountNoMutations());
		if(response.getCountNoMutations()!=0) {
			response.setRatio((double)response.getCountMutations() / response.getCountNoMutations());
		}else {
			response.setRatio((double)response.getCountNoMutations());
		}
		

		return response;
	}
    
	private Estadistica findEstadistica() {
		List<Estadistica> estadisticaList = estadisticaRepository.findAll();
		Estadistica estadistica = null;
		if (estadisticaList != null && !estadisticaList.isEmpty()) {
			estadistica = estadisticaList.get(0);
		} else {
			estadistica = new Estadistica();
			estadistica.setCountMutations(0);
			estadistica.setCountNoMutations(0);
		}
		return estadistica;
	}



	@Override
	public void updateEstadisticaMutacion() {
		Estadistica estadistica = findEstadistica();
		estadistica.setCountMutations(estadistica.getCountMutations() + 1);
		estadisticaRepository.save(estadistica);
		
	}

	@Override
	public void updateEstadisticaSinMutacion() {
		// TODO Auto-generated method stub
		Estadistica estadistica = findEstadistica();
		estadistica.setCountNoMutations(estadistica.getCountNoMutations() + 1);
		estadisticaRepository.save(estadistica);
	}
	

}
