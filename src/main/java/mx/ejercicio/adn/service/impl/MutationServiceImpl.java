package mx.ejercicio.adn.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ejercicio.adn.erros.DNABadRequestException;
import mx.ejercicio.adn.erros.DNASinMutacionException;
import mx.ejercicio.adn.model.Dna;
import mx.ejercicio.adn.repository.DnaRepository;
import mx.ejercicio.adn.service.EstadisticaService;
import mx.ejercicio.adn.service.MutationService;

@Service
public class MutationServiceImpl implements MutationService {

	@Autowired
	private DnaRepository dnaRepository;
	@Autowired
	private EstadisticaService estadisticaService;
	private static int numCoincidencias=4;

	@Override
	@Transactional
	public boolean hasMutation(String[] dna) throws DNABadRequestException, DNASinMutacionException {
		boolean hayMutacionHorizontal = false;
		boolean hayMutacionVertical = false;

		for (String fila : dna) {
			validarLetrasPermitidas(fila);
		}
		List<char[]> matrizDNA = arrayAMatriz(dna);
		hayMutacionHorizontal = hayMutacionHorizontal(matrizDNA);
		hayMutacionVertical = hayMutacionVertical(matrizDNA);
		if (!hayMutacionHorizontal && !hayMutacionVertical) {
			guardarDNA(dna, false);
			throw new DNASinMutacionException();
		}
		guardarDNA(dna, true);
	
		return false;
	}

	private void validarLetrasPermitidas(String fila) throws DNABadRequestException {
		boolean correcto = false;
		char[] letrasRecibidas = fila.toCharArray();
		for (char letraRecibida : letrasRecibidas) {
			switch (letraRecibida) {
			case 'A':
			case 'T':
			case 'C':
			case 'G':
				correcto = true;
				break;
			default:
				correcto = false;

			}
			if (!correcto) {
				throw new DNABadRequestException("Valor " + letraRecibida + " incorrecto");
			}
		}
	}

	private List<char[]> arrayAMatriz(String[] dna) {
		List<char[]> rows = new ArrayList<char[]>();
		for (String fila : dna) {
			rows.add(fila.toCharArray());
		}
		return rows;
	}

	private boolean hayMutacionHorizontal(List<char[]> matrizDNA) {
		boolean tieneMutacion = false;
		List<char[]> rows = matrizDNA;
		for (int i = 0; i < rows.size(); i++) {
			for (int j = 0; j < rows.get(i).length; j++) {
				int count = 1;
				for (int x = j + 1; x < rows.get(j).length; x++) {
					if (rows.get(i)[j] == rows.get(i)[x]) {
						count++;

					} else {
						break;
					}

				}
				tieneMutacion = hayMutacion(count, tieneMutacion);
			}

			if (tieneMutacion) {
				return tieneMutacion;
			}
		}
		return tieneMutacion;
	}
	
	private boolean hayMutacion(int count, boolean tieneMutacion) {
		if (count == numCoincidencias) {
			return  true;
		}
		return tieneMutacion;
	}

	/**
	 * @param dna
	 * @return
	 */
	private boolean hayMutacionVertical(List<char[]> matrizDNA) {
		boolean tieneMutacion = false;
		List<char[]> rows = matrizDNA;
		for (int i = 0; i < rows.size(); i++) {
			for (int j = 0; j < rows.get(i).length; j++) {
				int count = 1;
				for (int x = j + 1; x < rows.get(i).length; x++) {
					if (rows.get(j)[i] == rows.get(x)[i]) {
						count++;

					} else {
						break;
					}

				}
				tieneMutacion = hayMutacion(count, tieneMutacion);
			}

			// System.out.println("tieneMutacion " + tieneMutacion);
			if (tieneMutacion) {
				return tieneMutacion;
			}
		}
		return tieneMutacion;
	}
	
	private void guardarDNA(String[] dna, boolean tieneMutacion) {
		StringBuffer dnaSB = new StringBuffer();
		for (String fila : dna) {
			dnaSB.append(fila);
		}

		List<Dna> listDNA = dnaRepository.findByDnaString(dnaSB.toString());
		if (listDNA == null || listDNA.isEmpty()) {
			Dna dnaModel = new Dna();
			dnaModel.setDnaString(dnaSB.toString());
			dnaRepository.save(dnaModel);
			if (tieneMutacion) {
				estadisticaService.updateEstadisticaMutacion();
			} else {
				estadisticaService.updateEstadisticaSinMutacion();
			}

		}

	}
}
