package mx.ejercicio.adn.controller;

import org.springframework.http.ResponseEntity;

import mx.ejercicio.adn.dto.MutationRequest;

public interface MutationController {
	ResponseEntity<?> hasMutation(MutationRequest request);
}
