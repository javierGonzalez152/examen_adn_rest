package mx.ejercicio.adn.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.ejercicio.adn.model.Dna;

@Repository
public interface DnaRepository extends JpaRepository<Dna, Integer>{
	List<Dna> findByDnaString(String dnaString);
}
