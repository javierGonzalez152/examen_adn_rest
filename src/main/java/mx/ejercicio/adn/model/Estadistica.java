package mx.ejercicio.adn.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Estadistica {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer countNoMutations;
	private Integer countMutations;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCountNoMutations() {
		return countNoMutations;
	}

	public void setCountNoMutations(Integer countNoMutations) {
		this.countNoMutations = countNoMutations;
	}

	public Integer getCountMutations() {
		return countMutations;
	}

	public void setCountMutations(Integer countMutations) {
		this.countMutations = countMutations;
	}
}
