package mx.ejercicio.adn.erros;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Z046293
 *
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class DNASinMutacionException extends RuntimeException {
	public DNASinMutacionException() {
		super();
	}
}
