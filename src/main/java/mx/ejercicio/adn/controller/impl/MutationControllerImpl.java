package mx.ejercicio.adn.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import mx.ejercicio.adn.controller.MutationController;
import mx.ejercicio.adn.dto.MutationRequest;
import mx.ejercicio.adn.service.MutationService;

@RestController
public class MutationControllerImpl implements MutationController {

	@Autowired
	private MutationService mutationService;

	@Override
	@PostMapping("/mutation/")
	public ResponseEntity<?> hasMutation(@RequestBody MutationRequest request) {
		// TODO Auto-generated method stub
		System.out.print("Se ejecuta el micro servicios");
		mutationService.hasMutation(request.getDna());
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
