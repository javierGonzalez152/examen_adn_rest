package mx.ejercicio.adn.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.ejercicio.adn.model.Estadistica;

public interface EstadisticaRepository extends JpaRepository<Estadistica, Integer> {


}
